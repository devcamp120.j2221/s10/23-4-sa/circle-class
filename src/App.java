import com.devcamp.Circle;

public class App {
    public static void main(String[] args) throws Exception {
        //khai báo đối tượng
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(5.0);
        System.err.println(circle1.toString());
        System.err.println(circle2.toString());

        circle1.setRadius(10.0);
        System.err.println(circle1.toString());
        System.err.println(String.format("Dien tich = %f, Chu vi = %f", circle1.getArea(), circle1.getCircumference()));
    }
}
