package com.devcamp;

public class Circle {
    //Thuộc tính
    private double radius = 1;//khơi tạo cho thuộc tính

    /*
    * Hàm khởi tạo không tham số
    */
    public Circle() {
        super();
        this.radius = 1;
    }

    /*
    * Hàm khởi tạo có tham số
    * radius: bán kính hình tròn
    */    
    public Circle(double radius) {
        this.radius = radius;
    }

    //Hàm getter cho thuộc tính radius
    public double getRadius() {
        return this.radius;
    }

    //Hàm setter cho thuộc tính radius
    public void setRadius(double radius) {
        this.radius = radius;
    }

    //Hàm trả về diện tích hình tròn
    public double getArea() {
        //double area = Math.PI * this.radius * this.radius;
        double area = Math.PI * Math.pow(this.radius, 2);
        return area;
    }

    //Hàm trả về chu vi hình tròn
    public double getCircumference() {
        return 2 * Math.PI * this.radius;
    }

    //Hàm tostring
    public String toString() {
        //return "Circle[radius = " + this.radius + "]";
        //return String.format("Circle[radius = %s], dientich = %f, chuvi=%f", this.radius, this.getArea(), this.getCircumference());
        return String.format("Circle[radius = %s]", this.radius);
    }    
}
